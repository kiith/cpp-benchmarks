#include <cinttypes>
#include <cstdint>
#include <cstdio>

#include <algorithm>
#include <chrono>
#include <ctime>
#include <map>
#include <string>
#include <memory>

#ifdef __i386__
#define BENCH_X86
#endif
#ifdef __amd64__
#define BENCH_X86
#endif


#ifdef __linux
    #include <sys/resource.h>
#endif
#ifdef __unix
    #include <time.h>
#endif

#include "alloc.h"
#include "arithmetic.h"
#include "benchmark.h"
#include "exceptions.h"
#include "funcparams.h"
//#include "branch.h"
#include "polymorphism.h"
#include "synchronization.h"
#include "time.h"

static uint64_t cpu_freq(const char* const path)
{
#ifdef __linux
    FILE* file = fopen(path, "r");
    if (0 == file)
    {
        fprintf(stderr, "Failed to open '%s': %s", path, strerror(errno));
        return 0;
    }

    uint64_t result = 0;
    int status = fscanf(file, "%8" PRIu64, &result);

    if (ferror(file))
    {
        fprintf(stderr, "Error reading '%s': %s", path, strerror(errno));
    }
    fclose(file);

    return status == 1 ? result : 0;
#else
    return 0;
#endif
}

using ComputeColumn = std::string (*)(uint64_t, uint64_t);

struct BenchmarkData
{
    std::string name;
    std::shared_ptr<Benchmark> benchmark;
    uint32_t itersmult_rshift = 0;

    std::vector<ComputeColumn> extra_columns;

    BenchmarkData& col(ComputeColumn func)
    {
        extra_columns.push_back(func);
        return *this;
    }

    explicit BenchmarkData(const char* const name, Benchmark* benchmark, uint32_t iters_rshift = 0) noexcept
        : name(name)
        , benchmark(benchmark)
        , itersmult_rshift(iters_rshift)
    {
    }
};

void bench_series(const char* const name,
                  std::vector<BenchmarkData>& benchmarks,
                  const char* const freq_path,
                  const uint32_t iters_mult,
                  const double large, const double huge)
{
    const auto freq = cpu_freq(freq_path);
    const double ns_per_cycle = freq != 0 ? 1000000.0 / freq : 0;

    const auto red    = "\033[0;31m";
    const auto yellow = "\033[0;33m";
    const auto reset  = "\033[0m";
    const auto none   = "";

    puts("------------------------------------------------------------------------");
    printf("%s @ %3.1f Mhz (%s%.1f%s, %s%.1f%s)\n", name, freq / 1000.0,
           yellow, large, reset, red, huge, reset);
    puts("------------------------------------------------------------------------");
    fflush(stdout);

    for (auto& bench: benchmarks)
    {
        bench.benchmark->init();

        const auto imult = std::max(1u, iters_mult >> bench.itersmult_rshift);

        auto start = std::chrono::high_resolution_clock::now();
        bench.benchmark->run(imult);
        auto end = std::chrono::high_resolution_clock::now();

        bench.benchmark->deinit();

        auto ns = std::chrono::nanoseconds(end - start);
        const uint64_t iters         = imult * Benchmark::iters_base;
        const double ns_event = (ns.count() * 1000 / iters) / 1000.0;
        const double msecs           = (ns.count() * 1000 / 1000000) / 1000.0;
        const double cycles_event =
            ns_per_cycle != 0 ? ns_event / ns_per_cycle : 0;

        const auto col = cycles_event < large ? none
                       : cycles_event < huge ? yellow
                       : red;
        const auto rst = cycles_event < large ? none : reset;

        printf("%s: %9.1fns (%s%9.1fc%s) x %8" PRIu64" (%9.2fms)",
               bench.name.c_str(), ns_event,\
               col, cycles_event, rst,
               iters, msecs);
        for (auto func: bench.extra_columns)
        {
            std::string col = func(iters, ns.count());
            printf(" %12s", col.c_str());
        }
        printf("\n");
        fflush(stdout);
    }
    printf("\n\n");
    fflush(stdout);
}

template <uint64_t BytesPerIteration>
std::string B_ns(const uint64_t iters, const uint64_t ns)
{
    return std::to_string(iters * BytesPerIteration / ns) + " B/ns";
}

void warmup()
{
    puts("Warming up...");
    uint64_t dummy = 0;
    for (uint32_t i = 0; i < 0x1FFFFFFF; ++i)
    {
        dummy += i;
    }
    printf("Warmup done, dummy result: %" PRIu64"\n", dummy);
}

int benchmark_main(int argc, const char *argv[])
{
    const char* freq_path = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq";
    uint32_t iter_mult = 8192;
    if (argc > 1)
    {
        iter_mult = strtol(argv[1], NULL, 10);
        if (errno)
        {
            perror("CLI parsing error (need one argument - iteration multiplier as unsigned int)");
            return false;
        }
    }
    if (argc > 2)
    {
        freq_path = argv[2];
    }

    std::vector<BenchmarkData> polymorphism =
    {
        BenchmarkData("static               ", new BenchPolyStatic()),
        BenchmarkData("static-inlined       ", new BenchPolyStaticInlined()),
        BenchmarkData("fptr-predictable     ", new BenchPolyFPtr(Predictable::Yes)),
        BenchmarkData("fptr-unpredictable   ", new BenchPolyFPtr(Predictable::No)),
        BenchmarkData("virtual-predictable  ", new BenchPolyVirtual(Predictable::Yes)),
        BenchmarkData("virtual-unpredictable", new BenchPolyVirtual(Predictable::No))
    };

    // std::vector<BenchmarkData> branch =
    // {
    //     BenchmarkData("constant ", new BenchBranch(pattern_constant)),
    //     BenchmarkData("alternate", new BenchBranch(pattern_alternate)),
    //     BenchmarkData("random   ", new BenchBranch(pattern_random))
    // };

    std::vector<BenchmarkData> sync =
    {
        BenchmarkData("no sync (1 thread) ", new BenchNoSync(0)),
        BenchmarkData("no sync (2 threads)", new BenchNoSync(1)),
        BenchmarkData("no sync (3 threads)", new BenchNoSync(2)),
        BenchmarkData("no sync (4 threads)", new BenchNoSync(3)),
        BenchmarkData("mutex (1 thread)   ", new BenchMutex(0)),
        BenchmarkData("mutex (2 threads)  ", new BenchMutex(1)),
        BenchmarkData("mutex (3 threads)  ", new BenchMutex(2)),
        BenchmarkData("mutex (4 threads)  ", new BenchMutex(3)),
        BenchmarkData("atomic (1 thread)  ", new BenchAtomic(0)),
        BenchmarkData("atomic (2 threads) ", new BenchAtomic(1)),
        BenchmarkData("atomic (3 threads) ", new BenchAtomic(2)),
        BenchmarkData("atomic (4 threads) ", new BenchAtomic(3))
    };



    std::vector<BenchmarkData> ops =
    {
        BenchmarkData("+ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Add, true)),
        BenchmarkData("+ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Add, true)),
        BenchmarkData("+ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Add, true)),
        BenchmarkData("+ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Add, true)),
        BenchmarkData("+ (int32)  ", new BenchArithmetic<int32_t>(OpArith::Add, true)),
        BenchmarkData("+ (int64)  ", new BenchArithmetic<int64_t>(OpArith::Add, true)),
        BenchmarkData("+ (float)  ", new BenchArithmetic<float>(OpArith::Add, true)),
        BenchmarkData("+ (double) ", new BenchArithmetic<double>(OpArith::Add, true)),
#ifdef BENCH_X86
        BenchmarkData("+ (ldouble)", new BenchArithmetic<long double>(OpArith::Add, true)),
#endif

        BenchmarkData("- (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Sub, true)),
        BenchmarkData("- (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Sub, true)),
        BenchmarkData("- (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Sub, true)),
        BenchmarkData("- (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Sub, true)),
        BenchmarkData("- (int32)  ", new BenchArithmetic<int32_t>(OpArith::Sub, true)),
        BenchmarkData("- (int64)  ", new BenchArithmetic<int64_t>(OpArith::Sub, true)),
        BenchmarkData("- (float)  ", new BenchArithmetic<float>(OpArith::Sub, true)),
        BenchmarkData("- (double) ", new BenchArithmetic<double>(OpArith::Sub, true)),
#ifdef BENCH_X86
        BenchmarkData("- (ldouble)", new BenchArithmetic<long double>(OpArith::Sub, true)),
#endif

        BenchmarkData("* (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Mul, true)),
        BenchmarkData("* (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Mul, true)),
        BenchmarkData("* (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Mul, true)),
        BenchmarkData("* (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Mul, true)),
        BenchmarkData("* (int32)  ", new BenchArithmetic<int32_t>(OpArith::Mul, true)),
        BenchmarkData("* (int64)  ", new BenchArithmetic<int64_t>(OpArith::Mul, true)),
        BenchmarkData("* (float)  ", new BenchArithmetic<float>(OpArith::Mul, true)),
        BenchmarkData("* (double) ", new BenchArithmetic<double>(OpArith::Mul, true)),
#ifdef BENCH_X86
        BenchmarkData("* (ldouble)", new BenchArithmetic<long double>(OpArith::Mul, true)),
#endif

        BenchmarkData("/ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Div, true)),
        BenchmarkData("/ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Div, true)),
        BenchmarkData("/ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Div, true)),
        BenchmarkData("/ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Div, true)),
        BenchmarkData("/ (int32)  ", new BenchArithmetic<int32_t>(OpArith::Div, true)),
        BenchmarkData("/ (int64)  ", new BenchArithmetic<int64_t>(OpArith::Div, true)),
        BenchmarkData("/ (float)  ", new BenchArithmetic<float>(OpArith::Div, true)),
        BenchmarkData("/ (double) ", new BenchArithmetic<double>(OpArith::Div, true)),
#ifdef BENCH_X86
        BenchmarkData("/ (ldouble)", new BenchArithmetic<long double>(OpArith::Div, true)),
#endif

        BenchmarkData("% (uint8)     ", new BenchArithmetic<uint8_t>(OpArith::Mod, true)),
        BenchmarkData("% (uint16)    ", new BenchArithmetic<uint16_t>(OpArith::Mod, true)),
        BenchmarkData("% (uint32)    ", new BenchArithmetic<uint32_t>(OpArith::Mod, true)),
        BenchmarkData("% (uint64)    ", new BenchArithmetic<uint64_t>(OpArith::Mod, true)),
        BenchmarkData("% (int32)     ", new BenchArithmetic<int32_t>(OpArith::Mod, true)),
        BenchmarkData("% (int64)     ", new BenchArithmetic<int64_t>(OpArith::Mod, true)),
        BenchmarkData("fmod (float)  ", new BenchArithmetic<float>(OpArith::Mod, true)),
        BenchmarkData("fmod (double) ", new BenchArithmetic<double>(OpArith::Mod, true)),
#ifdef BENCH_X86
        BenchmarkData("fmod (ldouble)", new BenchArithmetic<long double>(OpArith::Mod, true)),
#endif

        BenchmarkData("*+ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::MAdd, true)),
        BenchmarkData("*+ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::MAdd, true)),
        BenchmarkData("*+ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::MAdd, true)),
        BenchmarkData("*+ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::MAdd, true)),
        BenchmarkData("*+ (int32)  ", new BenchArithmetic<int32_t>(OpArith::MAdd, true)),
        BenchmarkData("*+ (int64)  ", new BenchArithmetic<int64_t>(OpArith::MAdd, true)),
        BenchmarkData("*+ (float)  ", new BenchArithmetic<float>(OpArith::MAdd, true)),
        BenchmarkData("*+ (double) ", new BenchArithmetic<double>(OpArith::MAdd, true)),
#ifdef BENCH_X86
        BenchmarkData("*+ (ldouble)", new BenchArithmetic<long double>(OpArith::MAdd, true)),
#endif

        BenchmarkData("*+-/ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (int32)  ", new BenchArithmetic<int32_t>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (int64)  ", new BenchArithmetic<int64_t>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (float)  ", new BenchArithmetic<float>(OpArith::Mix, true)),
        BenchmarkData("*+-/ (double) ", new BenchArithmetic<double>(OpArith::Mix, true)),
#ifdef BENCH_X86
        BenchmarkData("*+-/ (ldouble)", new BenchArithmetic<long double>(OpArith::Mix, true)),
#endif
    };

    std::vector<BenchmarkData> thru =
    {
        BenchmarkData("+ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Add, false)),
        BenchmarkData("+ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Add, false)),
        BenchmarkData("+ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Add, false)),
        BenchmarkData("+ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Add, false)),
        BenchmarkData("+ (int32)  ", new BenchArithmetic<int32_t>(OpArith::Add, false)),
        BenchmarkData("+ (int64)  ", new BenchArithmetic<int64_t>(OpArith::Add, false)),
        BenchmarkData("+ (float)  ", new BenchArithmetic<float>(OpArith::Add, false)),
        BenchmarkData("+ (double) ", new BenchArithmetic<double>(OpArith::Add, false)),
#ifdef BENCH_X86
        BenchmarkData("+ (ldouble)", new BenchArithmetic<long double>(OpArith::Add, false)),
#endif

        BenchmarkData("- (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Sub, false)),
        BenchmarkData("- (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Sub, false)),
        BenchmarkData("- (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Sub, false)),
        BenchmarkData("- (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Sub, false)),
        BenchmarkData("- (int32)  ", new BenchArithmetic<int32_t>(OpArith::Sub, false)),
        BenchmarkData("- (int64)  ", new BenchArithmetic<int64_t>(OpArith::Sub, false)),
        BenchmarkData("- (float)  ", new BenchArithmetic<float>(OpArith::Sub, false)),
        BenchmarkData("- (double) ", new BenchArithmetic<double>(OpArith::Sub, false)),
#ifdef BENCH_X86
        BenchmarkData("- (ldouble)", new BenchArithmetic<long double>(OpArith::Sub, false)),
#endif

        BenchmarkData("* (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Mul, false)),
        BenchmarkData("* (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Mul, false)),
        BenchmarkData("* (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Mul, false)),
        BenchmarkData("* (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Mul, false)),
        BenchmarkData("* (int32)  ", new BenchArithmetic<int32_t>(OpArith::Mul, false)),
        BenchmarkData("* (int64)  ", new BenchArithmetic<int64_t>(OpArith::Mul, false)),
        BenchmarkData("* (float)  ", new BenchArithmetic<float>(OpArith::Mul, false)),
        BenchmarkData("* (double) ", new BenchArithmetic<double>(OpArith::Mul, false)),
#ifdef BENCH_X86
        BenchmarkData("* (ldouble)", new BenchArithmetic<long double>(OpArith::Mul, false)),
#endif

        BenchmarkData("/ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Div, false)),
        BenchmarkData("/ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Div, false)),
        BenchmarkData("/ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Div, false)),
        BenchmarkData("/ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Div, false)),
        BenchmarkData("/ (int32)  ", new BenchArithmetic<int32_t>(OpArith::Div, false)),
        BenchmarkData("/ (int64)  ", new BenchArithmetic<int64_t>(OpArith::Div, false)),
        BenchmarkData("/ (float)  ", new BenchArithmetic<float>(OpArith::Div, false)),
        BenchmarkData("/ (double) ", new BenchArithmetic<double>(OpArith::Div, false)),
#ifdef BENCH_X86
        BenchmarkData("/ (ldouble)", new BenchArithmetic<long double>(OpArith::Div, false)),
#endif

        BenchmarkData("% (uint8)     ", new BenchArithmetic<uint8_t>(OpArith::Mod, false)),
        BenchmarkData("% (uint16)    ", new BenchArithmetic<uint16_t>(OpArith::Mod, false)),
        BenchmarkData("% (uint32)    ", new BenchArithmetic<uint32_t>(OpArith::Mod, false)),
        BenchmarkData("% (uint64)    ", new BenchArithmetic<uint64_t>(OpArith::Mod, false)),
        BenchmarkData("% (int32)     ", new BenchArithmetic<int32_t>(OpArith::Mod, false)),
        BenchmarkData("% (int64)     ", new BenchArithmetic<int64_t>(OpArith::Mod, false)),
        BenchmarkData("fmod (float)  ", new BenchArithmetic<float>(OpArith::Mod, false)),
        BenchmarkData("fmod (double) ", new BenchArithmetic<double>(OpArith::Mod, false)),
#ifdef BENCH_X86
        BenchmarkData("fmod (ldouble)", new BenchArithmetic<long double>(OpArith::Mod, false)),
#endif

        BenchmarkData("*+ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::MAdd, false)),
        BenchmarkData("*+ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::MAdd, false)),
        BenchmarkData("*+ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::MAdd, false)),
        BenchmarkData("*+ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::MAdd, false)),
        BenchmarkData("*+ (int32)  ", new BenchArithmetic<int32_t>(OpArith::MAdd, false)),
        BenchmarkData("*+ (int64)  ", new BenchArithmetic<int64_t>(OpArith::MAdd, false)),
        BenchmarkData("*+ (float)  ", new BenchArithmetic<float>(OpArith::MAdd, false)),
        BenchmarkData("*+ (double) ", new BenchArithmetic<double>(OpArith::MAdd, false)),
#ifdef BENCH_X86
        BenchmarkData("*+ (ldouble)", new BenchArithmetic<long double>(OpArith::MAdd, false)),
#endif

        BenchmarkData("*+-/ (uint8)  ", new BenchArithmetic<uint8_t>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (uint16) ", new BenchArithmetic<uint16_t>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (uint32) ", new BenchArithmetic<uint32_t>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (uint64) ", new BenchArithmetic<uint64_t>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (int32)  ", new BenchArithmetic<int32_t>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (int64)  ", new BenchArithmetic<int64_t>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (float)  ", new BenchArithmetic<float>(OpArith::Mix, false)),
        BenchmarkData("*+-/ (double) ", new BenchArithmetic<double>(OpArith::Mix, false)),
#ifdef BENCH_X86
        BenchmarkData("*+-/ (ldouble)", new BenchArithmetic<long double>(OpArith::Mix, false)),
#endif
    };
    std::vector<BenchmarkData> func_params =
    {
        BenchmarkData("3   ", new BenchFuncParams(3)),
        BenchmarkData("4   ", new BenchFuncParams(4)),
        BenchmarkData("5   ", new BenchFuncParams(5)),
        BenchmarkData("15  ", new BenchFuncParams(15)),
        BenchmarkData("16  ", new BenchFuncParams(16)),
        BenchmarkData("17  ", new BenchFuncParams(17))
    };


    // less than 64k * 8k == 512M of memory
    const uint32_t frag_count = 65536;
    const uint32_t frag_max   = 8192;

    std::vector<BenchmarkData> malloc =
    {
        BenchmarkData("16   ", benchAlloc(malloc_plain<16>)).col(B_ns<16>),
        BenchmarkData("64   ", benchAlloc(malloc_plain<64>)).col(B_ns<64>),
        BenchmarkData("4k   ", benchAlloc(malloc_plain<4096>)).col(B_ns<4096>),
        BenchmarkData("16k  ", benchAlloc(malloc_plain<16 * 1024>)).col(B_ns<16 * 1024>),
        BenchmarkData("512k ", benchAlloc(malloc_plain<512 * 1024>), 4).col(B_ns<512 * 1024>),
        BenchmarkData("2M   ", benchAlloc(malloc_plain<2 * 1024 * 1024>), 4).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M  ", benchAlloc(malloc_plain<31 * 1024 * 1024>), 4).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M  ", benchAlloc(malloc_plain<32 * 1024 * 1024>), 4).col(B_ns<32 * 1024 * 1024>)
    };

    std::vector<BenchmarkData> malloc_a =
    {
        BenchmarkData("16    ", benchAlloc(malloc_access<16>)).col(B_ns<16>),
        BenchmarkData("64    ", benchAlloc(malloc_access<64>)).col(B_ns<64>),
        BenchmarkData("4k    ", benchAlloc(malloc_access<4096>)).col(B_ns<4096>),
        BenchmarkData("16k   ", benchAlloc(malloc_access<16 * 1024>)).col(B_ns<16 * 1024>),
        BenchmarkData("512k  ", benchAlloc(malloc_access<512 * 1024>), 13).col(B_ns<512 * 1024>),
        BenchmarkData("2M    ", benchAlloc(malloc_access<2 * 1024 * 1024>), 13).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M   ", benchAlloc(malloc_access<31 * 1024 * 1024>), 13).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M   ", benchAlloc(malloc_access<32 * 1024 * 1024>), 13).col(B_ns<32 * 1024 * 1024>)
    };

    std::vector<BenchmarkData> new_a =
    {
        BenchmarkData("16    ", benchAlloc(new_access<16>)).col(B_ns<16>),
        BenchmarkData("64    ", benchAlloc(new_access<64>)).col(B_ns<64>),
        BenchmarkData("4k    ", benchAlloc(new_access<4096>)).col(B_ns<4096>),
        BenchmarkData("16k   ", benchAlloc(new_access<16 * 1024>)).col(B_ns<16 * 1024>),
        BenchmarkData("512k  ", benchAlloc(new_access<512 * 1024>), 13).col(B_ns<512 * 1024>),
        BenchmarkData("2M    ", benchAlloc(new_access<2 * 1024 * 1024>), 13).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M   ", benchAlloc(new_access<31 * 1024 * 1024>), 13).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M   ", benchAlloc(new_access<32 * 1024 * 1024>), 13).col(B_ns<32 * 1024 * 1024>)
    };

    std::vector<BenchmarkData> new_ctor =
    {
        BenchmarkData("16    ", benchFunc(::new_ctor<16>)).col(B_ns<16>),
        BenchmarkData("64    ", benchFunc(::new_ctor<64>)).col(B_ns<64>),
        BenchmarkData("4k    ", benchFunc(::new_ctor<4096>)).col(B_ns<4096>),
        BenchmarkData("16k   ", benchFunc(::new_ctor<16 * 1024>)).col(B_ns<16 * 1024>),
        BenchmarkData("512k  ", benchFunc(::new_ctor<512 * 1024>), 13).col(B_ns<512 * 1024>),
        BenchmarkData("2M    ", benchFunc(::new_ctor<2 * 1024 * 1024>), 13).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M   ", benchFunc(::new_ctor<31 * 1024 * 1024>), 13).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M   ", benchFunc(::new_ctor<32 * 1024 * 1024>), 13).col(B_ns<32 * 1024 * 1024>)
    };

    std::vector<BenchmarkData> frag_malloc =
    {
        BenchmarkData("16   ", benchAlloc(malloc_plain<16>, frag_count, frag_max)).col(B_ns<16>),
        BenchmarkData("64   ", benchAlloc(malloc_plain<64>, frag_count, frag_max)).col(B_ns<64>),
        BenchmarkData("4k   ", benchAlloc(malloc_plain<4096>, frag_count, frag_max)).col(B_ns<4096>),
        BenchmarkData("16k  ", benchAlloc(malloc_plain<16 * 1024>, frag_count, frag_max)).col(B_ns<16 * 1024>),
        BenchmarkData("512k ", benchAlloc(malloc_plain<512 * 1024>, frag_count, frag_max), 4).col(B_ns<512 * 1024>),
        BenchmarkData("2M   ", benchAlloc(malloc_plain<2 * 1024 * 1024>, frag_count, frag_max), 4).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M  ", benchAlloc(malloc_plain<31 * 1024 * 1024>, frag_count, frag_max), 4).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M  ", benchAlloc(malloc_plain<32 * 1024 * 1024>, frag_count, frag_max), 4).col(B_ns<32 * 1024 * 1024>)
    };

    std::vector<BenchmarkData> frag_malloc_a =
    {
        BenchmarkData("16    ", benchAlloc(malloc_access<16>, frag_count, frag_max)).col(B_ns<16>),
        BenchmarkData("64    ", benchAlloc(malloc_access<64>, frag_count, frag_max)).col(B_ns<64>),
        BenchmarkData("4k    ", benchAlloc(malloc_access<4096>, frag_count, frag_max)).col(B_ns<4096>),
        BenchmarkData("16k   ", benchAlloc(malloc_access<16 * 1024>, frag_count, frag_max)).col(B_ns<16 * 1024>),
        BenchmarkData("512k  ", benchAlloc(malloc_access<512 * 1024>, frag_count, frag_max), 13).col(B_ns<512 * 1024>),
        BenchmarkData("2M    ", benchAlloc(malloc_access<2 * 1024 * 1024>, frag_count, frag_max), 13).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M   ", benchAlloc(malloc_access<31 * 1024 * 1024>, frag_count, frag_max), 13).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M   ", benchAlloc(malloc_access<32 * 1024 * 1024>, frag_count, frag_max), 13).col(B_ns<32 * 1024 * 1024>)
    };


    std::vector<BenchmarkData> stack_a =
    {
        BenchmarkData("16    ", benchFunc(stack_access<16>)).col(B_ns<16>),
        BenchmarkData("64    ", benchFunc(stack_access<64>)).col(B_ns<64>),
        BenchmarkData("4k    ", benchFunc(stack_access<4096>)).col(B_ns<4096>),
        BenchmarkData("16k   ", benchFunc(stack_access<16 * 1024>)).col(B_ns<16 * 1024>),
        BenchmarkData("512k  ", benchFunc(stack_access<512 * 1024>), 13).col(B_ns<512 * 1024>),
        BenchmarkData("2M    ", benchFunc(stack_access<2 * 1024 * 1024>), 13).col(B_ns<2 * 1024 * 1024>),
        BenchmarkData("31M   ", benchFunc(stack_access<31 * 1024 * 1024>), 13).col(B_ns<31 * 1024 * 1024>),
        BenchmarkData("32M   ", benchFunc(stack_access<32 * 1024 * 1024>), 13).col(B_ns<32 * 1024 * 1024>)
    };

    std::vector<BenchmarkData> time =
    {
        BenchmarkData("high_resolution_clock   ", benchFunc(std::chrono::high_resolution_clock::now)),
        BenchmarkData("steady_clock            ", benchFunc(std::chrono::steady_clock::now)),
        BenchmarkData("system_clock            ", benchFunc(std::chrono::system_clock::now)),
        BenchmarkData("cstdlib time            ", benchFunc(cstdlib_time)),
        BenchmarkData("cstdlib clock           ", benchFunc(clock)),
#ifdef __unix
        BenchmarkData("CLOCK_REALTIME          ", benchFunc(posix_clock_gettime<CLOCK_REALTIME>)),
        BenchmarkData("CLOCK_MONOTONIC         ", benchFunc(posix_clock_gettime<CLOCK_MONOTONIC>)),
        BenchmarkData("CLOCK_PROCESS_CPUTIME_ID", benchFunc(posix_clock_gettime<CLOCK_PROCESS_CPUTIME_ID>)),
        BenchmarkData("CLOCK_THREAD_CPUTIME_ID ", benchFunc(posix_clock_gettime<CLOCK_THREAD_CPUTIME_ID>)),
#endif
        BenchmarkData("CLOCK_REALTIME_COARSE   ", benchFunc(posix_clock_gettime<CLOCK_REALTIME_COARSE>)),
        BenchmarkData("CLOCK_MONOTONIC_COARSE  ", benchFunc(posix_clock_gettime<CLOCK_MONOTONIC_COARSE>)),
        BenchmarkData("CLOCK_MONOTONIC_RAW     ", benchFunc(posix_clock_gettime<CLOCK_MONOTONIC_RAW>)),
        BenchmarkData("CLOCK_BOOTTIME          ", benchFunc(posix_clock_gettime<CLOCK_BOOTTIME>)),
#ifdef __linux
#endif
    };

    std::vector<BenchmarkData> throwing =
    {
        BenchmarkData("nothrow 1/1      ", new BenchNoThrow(1)),
        BenchmarkData("nothrow 1/100000 ", new BenchNoThrow(10000)),
        BenchmarkData("throw 1/1        ", new BenchThrow(1)),
        BenchmarkData("throw 1/10       ", new BenchThrow(10)),
        BenchmarkData("throw 1/100      ", new BenchThrow(100)),
        BenchmarkData("throw 1/1000     ", new BenchThrow(1000)),
        BenchmarkData("throw 1/10000    ", new BenchThrow(10000)),
        BenchmarkData("throw 1/100000   ", new BenchThrow(100000)),
        BenchmarkData("throw 1/1000000  ", new BenchThrow(1000000)),
    };
    warmup();

    bench_series("Polymorphism",                       polymorphism, freq_path, iter_mult, 10, 20);
    // bench_series("Branch prediction",                  branch,       freq_path, iter_mult, 2, 4);
    bench_series("malloc()+free()",                    malloc,       freq_path, iter_mult, 250, 500);
    bench_series("(frag heap) malloc()+free()",        frag_malloc,       freq_path, iter_mult, 250, 500);
    bench_series("malloc()+1B/4k access+free()",       malloc_a,     freq_path, iter_mult, 250, 500);
    bench_series("(frag heap) malloc()+1B/4k access+free()",    frag_malloc_a,     freq_path, iter_mult, 250, 500);
    // bench_series("new+1B/4k access+delete[]",          new_a,        freq_path, iter_mult, 250, 500);
    bench_series("new+ctor+1B/4k access+delete[]",     new_ctor,     freq_path, iter_mult, 250, 500);
    bench_series("stack array 1B/4k access",           stack_a,      freq_path, iter_mult, 250, 500);
    bench_series("Time measurement",                   time,         freq_path, iter_mult, 50, 500);
    bench_series("Synchronization (of a 32bit value)", sync,         freq_path, iter_mult, 10, 100);

    bench_series("Op latency",                         ops,          freq_path, iter_mult, 8,  16);
    bench_series("Op throughput",                      thru,         freq_path, iter_mult, 8,  16);
    bench_series("Func params count (32bit/param)",    func_params,  freq_path, iter_mult, 1,  2);
    bench_series("Exception throwing (min unwinding)", throwing,     freq_path, iter_mult, 10, 100);

    return 0;
}

int main(int argc, const char *argv[])
{
#ifdef __linux
    const rlim_t stack_size = 48 * 1024 * 1024;   // min stack size = 48 MB
    struct rlimit rl;
    int result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0 && rl.rlim_cur < stack_size)
    {
        rl.rlim_cur = stack_size;
        result = setrlimit(RLIMIT_STACK, &rl);
        if (result != 0)
        {
            fprintf(stderr, "setrlimit returned result = %d\n", result);

        }
    }
#endif

    return benchmark_main(argc, argv);
}
