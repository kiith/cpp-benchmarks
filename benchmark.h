#ifndef BENCHMARK_H_6YJWRMVD
#define BENCHMARK_H_6YJWRMVD

#include <cstdint>

class Benchmark
{
public:
    static constexpr uint32_t iters_base = 256;

    virtual void init() {}

    virtual void deinit() {}

    virtual void run(const uint32_t iters_mult) = 0;

    virtual ~Benchmark() {}
};

template <typename Func>
class BenchFunc: public Benchmark
{
public:
    /** Construct a BenchFunc.
     *
     * @param func Function to benchmark.
     */
    explicit BenchFunc(Func* func) noexcept
        : func_(func)
    {
    }

    virtual void run(const uint32_t iters_mult)
    {
        const auto iters = iters_mult * iters_base / 8;
        for (uint32_t i = 0; i < iters; ++i)
        {
            func_(); func_(); func_(); func_();
            func_(); func_(); func_(); func_();
        }
    }

private:
    Func* func_;
};

template <typename Func>
BenchFunc<Func>* benchFunc(Func* func)
{
    return new BenchFunc<Func>(func);
}

#endif /* end of include guard: BENCHMARK_H_6YJWRMVD */
