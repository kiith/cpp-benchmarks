
#ifndef SYNCHRONIZATION_H_ZL6XJZXV
#define SYNCHRONIZATION_H_ZL6XJZXV

#include <atomic>
#include <mutex>
#include <thread>


class BenchSync: public Benchmark
{
public:
    explicit BenchSync(uint32_t extra_threads) noexcept
        : extra_threads_(extra_threads)
    {
    }

    virtual void run(const uint32_t iters_mult) override
    {
        std::vector<std::thread> threads;
        for (uint32_t i = 0; i < extra_threads_; ++i)
        {
            threads.push_back(std::thread(&BenchSync::run_internal, this, iters_mult));
        }

        run_internal(iters_mult);

        for (auto& thread: threads)
        {
            thread.join();
        }
    }

protected:
    virtual void run_internal(const uint32_t iters_mult) = 0;

private:
    uint32_t extra_threads_;
};

class BenchNoSync final: public BenchSync
{
public:
    using BenchSync::BenchSync;

protected:
    virtual void run_internal(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base;
        for (uint32_t i = 0; i < iters; ++i)
        {
            dummy_ += i;
        }
    }

    uint32_t dummy_;
};

class BenchMutex final: public BenchSync
{
public:
    using BenchSync::BenchSync;

protected:
    virtual void run_internal(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base / 8;
        for (uint32_t i = 0; i < iters; ++i)
        {
            std::lock_guard<std::mutex> guard(mutex_);
            dummy_ += i;
        }
    }

    std::mutex mutex_;

    uint32_t dummy_;
};

class BenchAtomic: public BenchSync
{
public:
    using BenchSync::BenchSync;

protected:
    virtual void run_internal(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base;
        for (uint32_t i = 0; i < iters; ++i)
        {
            dummy_ += i;
        }
    }

    std::atomic<uint32_t> dummy_;
};

#endif /* end of include guard: SYNCHRONIZATION_H_ZL6XJZXV */
