#ifndef ALLOC_H_C8EO2CVZ
#define ALLOC_H_C8EO2CVZ

#include <cstdint>
#include <cstring>

#include "benchmark.h"
#include "util.h"


struct Vec4
{
    /** Construct a Vec4.
     */
    explicit Vec4() noexcept
        : x(0)
        , y(0)
        , z(0)
        , w(1)
    {
    }

    float x, y, z, w;
};


void* malloc_wrap(const size_t size);
void free_wrap(void* const ptr);

uint8_t* new_uint8_t(const size_t size);
void delete_uint8_t(uint8_t* const ptr);

Vec4* new_vec4(const size_t size);
void delete_vec4(Vec4* const ptr);



template <uint32_t Size>
void malloc_plain()
{
    uint8_t* buffer = static_cast<uint8_t*>(malloc_wrap(Size));
    free_wrap(buffer);
}

template <uint32_t Size>
void malloc_access()
{
    uint8_t* buffer = static_cast<uint8_t*>(malloc_wrap(Size));
    for (uint32_t i = 0; i < Size; i += 4096)
    {
        buffer[i] = i & 0xFF;
    }
    free_wrap(buffer);
}

template <uint32_t Size>
void new_access()
{
    uint8_t* buffer = new_uint8_t(Size);
    for (uint32_t i = 0; i < Size; i += 4096)
    {
        buffer[i] = i & 0xFF;
    }
    delete_uint8_t(buffer);
}

template <uint32_t Size>
void new_ctor()
{
    constexpr uint32_t Count = Size / sizeof(Vec4);
    constexpr uint32_t Skip  = 4096 / sizeof(Vec4);
    Vec4* buffer = new_vec4(Count);
    for (uint32_t i = 0; i < Count; i += Skip)
    {
        buffer[i].x = i;
    }
    delete_vec4(buffer);
}



#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
template <uint32_t Size>
void stack_access()
{
    volatile uint8_t buffer[Size];
    for (uint32_t i = 0; i < Size; i += 4096)
    {
        buffer[i] = i & 0xFF;
    }
}
#pragma GCC diagnostic pop


template <typename Func>
class BenchAlloc final: public BenchFunc<Func>
{
public:
    explicit BenchAlloc(Func* func, uint32_t frag_count, uint32_t frag_max) noexcept
        : BenchFunc<Func>(func)
        , frag_count_(frag_count)
        , frag_max_(frag_max)
    {
    }

    virtual void init() override
    {
        // always the same seed so we get reproducible results 
        std::default_random_engine engine(42);
        std::uniform_int_distribution<uint32_t> distribution(1, frag_max_);
        for (uint32_t i = 0; i < frag_count_; ++i)
        {
            const uint32_t size = distribution(engine);
            void* ptr = malloc(size);
            memset(ptr, 42, size);
            frags_.push_back(ptr);
        }
    }

    virtual void deinit() override
    {
        for (auto& frag: frags_)
        {
            free(frag);
        }
        frags_.clear();
    }

private:
    uint32_t frag_count_;
    uint32_t frag_max_;

    std::vector<void*> frags_;
};

template <typename Func>
BenchFunc<Func>* benchAlloc(Func* func,
                            uint32_t frag_count = 0,
                            uint32_t frag_max   = 0)
{
    return new BenchAlloc<Func>(func, frag_count, frag_max);
}

#endif /* end of include guard: ALLOC_H_C8EO2CVZ */
