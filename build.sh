#!/bin/sh

CXX=${CXX:-g++}

SOURCES="main.cpp polymorphism.cpp alloc.cpp time.cpp funcparams.cpp exceptions.cpp"
OPTS="-std=c++14 -Wall -Wextra"

$CXX --version | head -1

echo "building debug"
$CXX $OPTS -g  -o benchmarks_debug $SOURCES -lpthread
echo "building O3"
$CXX $OPTS -O3 -o benchmarks_O3    $SOURCES -lpthread > /dev/null 2>&1
echo "building O2"
$CXX $OPTS -O2 -o benchmarks_O2    $SOURCES -lpthread > /dev/null 2>&1
echo "building Os"
$CXX $OPTS -Os -o benchmarks_Os    $SOURCES -lpthread > /dev/null 2>&1
