#ifndef ARITHMETIC_H_WUNPAGDN
#define ARITHMETIC_H_WUNPAGDN

#include "benchmark.h"

#include <cassert>
#include <cmath>

#include <type_traits>

enum class OpArith: uint32_t
{
    Div,
    Add,
    Sub,
    Mul,
    Mod,
    MAdd,
    Mix
};




template <typename I>
class BenchArithmetic final: public Benchmark
{
public:
    BenchArithmetic(OpArith op, const bool latency)
        : op_(op)
        , latency_(latency)
    {
    }

    virtual void init() override
    {
        std::random_device device;
        std::default_random_engine engine(device());
        std::uniform_int_distribution<uint32_t> distribution(1, 127);

        value_  = distribution(engine);

        value1_ = distribution(engine);
        value2_ = distribution(engine);
        value3_ = distribution(engine);
        value4_ = distribution(engine);
        value5_ = distribution(engine);
        value6_ = distribution(engine);
        value7_ = distribution(engine);
        value8_ = distribution(engine);
    }

    virtual void run(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * Benchmark::iters_base / 8;
        switch (op_)
        {
            case OpArith::Add:  runAdd(iters);    break;
            case OpArith::Sub:  runSub(iters);    break;
            case OpArith::Mul:  runMul(iters);    break;
            case OpArith::Div:  runDiv(iters);    break;
            case OpArith::Mod:  runMod(iters);    break;
            case OpArith::MAdd: runMulAdd(iters); break;
            case OpArith::Mix:  runMix(iters);    break;
            default: assert(false);
        }
    }

private:
    void runAdd(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ + value_;
            value_ = value_  + value2_;
            value_ = value3_ + value_;
            value_ = value_  + value4_;
            value_ = value5_ + value_;
            value_ = value_  + value6_;
            value_ = value7_ + value_;
            value_ = value_  + value8_;
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value1_ = value_ + value1_;
            value2_ = value_ + value2_;
            value3_ = value_ + value3_;
            value4_ = value_ + value4_;
            value5_ = value_ + value5_;
            value6_ = value_ + value6_;
            value7_ = value_ + value7_;
            value8_ = value_ + value8_;
        }
    }

    void runSub(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ - value_;
            value_ = value_  - value2_;
            value_ = value3_ - value_;
            value_ = value_  - value4_;
            value_ = value5_ - value_;
            value_ = value_  - value6_;
            value_ = value7_ - value_;
            value_ = value_  - value8_;
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value1_ = value_ - value1_;
            value2_ = value_ - value2_;
            value3_ = value_ - value3_;
            value4_ = value_ - value4_;
            value5_ = value_ - value5_;
            value6_ = value_ - value6_;
            value7_ = value_ - value7_;
            value8_ = value_ - value8_;
        }
    }

    void runMul(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ * value_;
            value_ = value_  * value2_;
            value_ = value3_ * value_;
            value_ = value_  * value4_;
            value_ = value5_ * value_;
            value_ = value_  * value6_;
            value_ = value7_ * value_;
            value_ = value_  * value8_;
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value1_ = value_ * value1_;
            value2_ = value_ * value2_;
            value3_ = value_ * value3_;
            value4_ = value_ * value4_;
            value5_ = value_ * value5_;
            value6_ = value_ * value6_;
            value7_ = value_ * value7_;
            value8_ = value_ * value8_;
        }
    }

    void runDiv(const uint32_t iters)
    {
        if(latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ / I(I(1) + value_);
            value_ = value_  / I(I(1) + value2_);
            value_ = value3_ / I(I(1) + value_);
            value_ = value_  / I(I(1) + value4_);
            value_ = value5_ / I(I(1) + value_);
            value_ = value_  / I(I(1) + value6_);
            value_ = value7_ / I(I(1) + value_);
            value_ = value_  / I(I(1) + value8_);
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            value1_ = value1_ / value_;
            value2_ = value2_ / value_;
            value3_ = value3_ / value_;
            value4_ = value4_ / value_;
            value5_ = value5_ / value_;
            value6_ = value6_ / value_;
            value7_ = value7_ / value_;
            value8_ = value8_ / value_;
        }
    }

    template<class Q = I, typename std::enable_if<
        std::is_integral<Q>::value>::type* = nullptr>
    void runMod(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ % I(I(1) + value_);
            value_ = value_  % I(I(1) + value2_);
            value_ = value3_ % I(I(1) + value_);
            value_ = value_  % I(I(1) + value4_);
            value_ = value5_ % I(I(1) + value_);
            value_ = value_  % I(I(1) + value6_);
            value_ = value7_ % I(I(1) + value_);
            value_ = value_  % I(I(1) + value8_);
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            value1_ = value1_ % value_;
            value2_ = value2_ % value_;
            value3_ = value3_ % value_;
            value4_ = value4_ % value_;
            value5_ = value5_ % value_;
            value6_ = value6_ % value_;
            value7_ = value7_ % value_;
            value8_ = value8_ % value_;
        }
    }

    static float fmod_t(const float a, const float b) { return fmodf(a, b); }
    static float fmod_t(const double a, const double b) { return fmod(a, b); }
    static float fmod_t(const long double a, const long double b) { return fmodl(a, b); }

    template<class Q = I, typename std::enable_if<
        !std::is_integral<Q>::value>::type* = nullptr>
    void runMod(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            value_ = fmod_t(value1_, I(I(1) + value_));
            value_ = fmod_t(value_ , I(I(1) + value2_));
            value_ = fmod_t(value3_, I(I(1) + value_));
            value_ = fmod_t(value_ , I(I(1) + value4_));
            value_ = fmod_t(value5_, I(I(1) + value_));
            value_ = fmod_t(value_ , I(I(1) + value6_));
            value_ = fmod_t(value7_, I(I(1) + value_));
            value_ = fmod_t(value_ , I(I(1) + value8_));
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            value1_ = fmod_t(value1_, value_);
            value2_ = fmod_t(value2_, value_);
            value3_ = fmod_t(value3_, value_);
            value4_ = fmod_t(value4_, value_);
            value5_ = fmod_t(value5_, value_);
            value6_ = fmod_t(value6_, value_);
            value7_ = fmod_t(value7_, value_);
            value8_ = fmod_t(value8_, value_);
        }
    }

    void runMulAdd(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ * value_;
            value_ = value_  + value2_;
            value_ = value3_ * value_;
            value_ = value_  + value4_;
            value_ = value5_ * value_;
            value_ = value_  + value6_;
            value_ = value7_ * value_;
            value_ = value_  + value8_;
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value1_ = value_ * value1_;
            value2_ = value_ + value2_;
            value3_ = value_ * value3_;
            value4_ = value_ + value4_;
            value5_ = value_ * value5_;
            value6_ = value_ + value6_;
            value7_ = value_ * value7_;
            value8_ = value_ + value8_;
        }
    }

    void runMix(const uint32_t iters)
    {
        if (latency_) for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value_ = value1_ * value_;
            value_ = value_  + value2_;
            value_ = value3_ - value_;
            value_ = value_  / value4_;
            value_ = value5_ * value_;
            value_ = value_  + value6_;
            value_ = value7_ - value_;
            value_ = value_  / value8_;
        }
        else for (uint32_t i = 0; i < iters; ++i)
        {
            // TODO check assembly to ensure there are really 8 ops
            value1_ = value_ * value1_;
            value2_ = value_ + value2_;
            value3_ = value_ - value3_;
            value4_ = value_ / (I(1) + value4_);
            value5_ = value_ * value5_;
            value6_ = value_ + value6_;
            value7_ = value_ - value7_;
            value8_ = value_ / (I(1) + value8_);
        }
    }


    volatile I value_ = 0;

    volatile I value1_;
    volatile I value2_;
    volatile I value3_;
    volatile I value4_;
    volatile I value5_;
    volatile I value6_;
    volatile I value7_;
    volatile I value8_;

    OpArith op_;
    bool latency_;
};


#endif /* end of include guard: ARITHMETIC_H_WUNPAGDN */
