#ifndef FUNCPARAMS_H_FQWKD41W
#define FUNCPARAMS_H_FQWKD41W

#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include "benchmark.h"

uint32_t params_3(uint32_t a, uint32_t b, uint32_t c);
uint32_t params_4(uint32_t a, uint32_t b, uint32_t c, uint32_t d);
uint32_t params_5(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e);

uint32_t params_15(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e,
                   uint32_t f, uint32_t g, uint32_t h, uint32_t i, uint32_t j,
                   uint32_t k, uint32_t l, uint32_t m, uint32_t n, uint32_t o);
uint32_t params_16(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e,
                   uint32_t f, uint32_t g, uint32_t h, uint32_t i, uint32_t j,
                   uint32_t k, uint32_t l, uint32_t m, uint32_t n, uint32_t o,
                   uint32_t p);
uint32_t params_17(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e,
                   uint32_t f, uint32_t g, uint32_t h, uint32_t i, uint32_t j,
                   uint32_t k, uint32_t l, uint32_t m, uint32_t n, uint32_t o,
                   uint32_t p, uint32_t q);



class BenchFuncParams final: public Benchmark
{
public:
    explicit BenchFuncParams(uint32_t param_count) noexcept
        : param_count_(param_count)
    {
    }

    virtual void run(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base / 8;
        switch (param_count_)
        {
            case 3:  run_3(iters);  break;
            case 4:  run_4(iters);  break;
            case 5:  run_5(iters);  break;
            case 15: run_15(iters); break;
            case 16: run_16(iters); break;
            case 17: run_17(iters); break;
            default:
                printf("version with %u params not implemented\n", param_count_);
                fflush(stdout);
                abort();
        }
    }

private:
    void run_3(const uint32_t iters)
    {
        uint32_t a = 0;
        uint32_t b = 1;
        uint32_t c = 2;
        for (uint32_t i = 0; i < iters; ++i)
        {
            a = params_3(a, b, c);
            b = params_3(a, b, c);
            c = params_3(a, b, c);
            b = params_3(a, b, c);
            c = params_3(a, b, c);
            a = params_3(a, b, c);
            c = params_3(a, b, c);
            a = params_3(a, b, c);
        }
    }

    void run_4(const uint32_t iters)
    {
        uint32_t a = 0;
        uint32_t b = 1;
        uint32_t c = 2;
        uint32_t d = 3;
        for (uint32_t i = 0; i < iters; ++i)
        {
            a = params_4(a, b, c, d);
            b = params_4(a, b, c, d);
            c = params_4(a, b, c, d);
            d = params_4(a, b, c, d);
            a = params_4(a, b, c, d);
            b = params_4(a, b, c, d);
            c = params_4(a, b, c, d);
            d = params_4(a, b, c, d);
        }
    }

    void run_5(const uint32_t iters)
    {
        uint32_t a = 0;
        uint32_t b = 1;
        uint32_t c = 2;
        uint32_t d = 3;
        uint32_t e = 4;
        for (uint32_t i = 0; i < iters; ++i)
        {
            a = params_5(a, b, c, d, e);
            b = params_5(a, b, c, d, e);
            c = params_5(a, b, c, d, e);
            d = params_5(a, b, c, d, e);
            e = params_5(a, b, c, d, e);
            a = params_5(a, b, c, d, e);
            b = params_5(a, b, c, d, e);
            c = params_5(a, b, c, d, e);
        }
    }

    void run_15(const uint32_t iters)
    {
        uint32_t a = 0;
        uint32_t b = 1;
        uint32_t c = 2;
        uint32_t d = 3;
        uint32_t e = 4;
        uint32_t f = 5;
        uint32_t g = 6;
        uint32_t h = 7;
        uint32_t i = 8;
        uint32_t j = 9;
        uint32_t k = 10;
        uint32_t l = 11;
        uint32_t m = 12;
        uint32_t n = 13;
        uint32_t o = 14;
        for (uint32_t it = 0; it < iters; ++it)
        {
            a = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            b = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            c = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            d = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            e = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            f = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            g = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
            h = params_15(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o);
        }
    }

    void run_16(const uint32_t iters)
    {
        uint32_t a = 0;
        uint32_t b = 1;
        uint32_t c = 2;
        uint32_t d = 3;
        uint32_t e = 4;
        uint32_t f = 5;
        uint32_t g = 6;
        uint32_t h = 7;
        uint32_t i = 8;
        uint32_t j = 9;
        uint32_t k = 10;
        uint32_t l = 11;
        uint32_t m = 12;
        uint32_t n = 13;
        uint32_t o = 14;
        uint32_t p = 15;
        for (uint32_t it = 0; it < iters; ++it)
        {
            a = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            b = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            c = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            d = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            e = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            f = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            g = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
            h = params_16(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);
        }
    }

    void run_17(const uint32_t iters)
    {
        uint32_t a = 0;
        uint32_t b = 1;
        uint32_t c = 2;
        uint32_t d = 3;
        uint32_t e = 4;
        uint32_t f = 5;
        uint32_t g = 6;
        uint32_t h = 7;
        uint32_t i = 8;
        uint32_t j = 9;
        uint32_t k = 10;
        uint32_t l = 11;
        uint32_t m = 12;
        uint32_t n = 13;
        uint32_t o = 14;
        uint32_t p = 15;
        uint32_t q = 16;
        for (uint32_t it = 0; it < iters; ++it)
        {
            a = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            b = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            c = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            d = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            e = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            f = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            g = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            h = params_17(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
        }
    }

    uint32_t param_count_;
};



#endif /* end of include guard: FUNCPARAMS_H_FQWKD41W */
