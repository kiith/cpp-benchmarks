#ifndef EXCEPTIONS_H_3XJNGS2F
#define EXCEPTIONS_H_3XJNGS2F

#include "benchmark.h"


class DummyException
{
public:
    /// Construct a DummyException with specified error message.
    DummyException(const char* const message) : msg(message) {}

    const char* const msg;
};

void will_not_throw(uint32_t& idx, const uint32_t period) noexcept;

void may_throw(uint32_t& idx, const uint32_t period);

class BenchNoThrow final: public Benchmark
{
public:
    explicit BenchNoThrow(uint32_t dummy_period) noexcept
        : dummy_period_(dummy_period)
    {
    }

    virtual void run(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base;
        uint32_t i_in_period = 1;
        for (uint32_t i = 0; i < iters; ++i)
        {
            will_not_throw(i_in_period, dummy_period_);
        }
    }

private:
    uint32_t dummy_period_;
};

class BenchThrow final: public Benchmark
{
public:
    explicit BenchThrow(uint32_t throw_period) noexcept
        : throw_period_(throw_period)
    {
    }

    virtual void run(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base;
        uint32_t i_in_period = 1;
        for (uint32_t i = 0; i < iters; ++i)
        {
            try
            {
                may_throw(i_in_period, throw_period_);
            }
            catch(DummyException& e)
            {
                dummy_++;
            }
        }
    }

private:
    uint32_t throw_period_;
    uint32_t dummy_ = 0;
};


#endif /* end of include guard: EXCEPTIONS_H_3XJNGS2F */
