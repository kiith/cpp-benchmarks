#include "funcparams.h"

uint32_t params_3(uint32_t a, uint32_t b, uint32_t c)
{
    return a + b + c;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
uint32_t params_4(uint32_t a, uint32_t b, uint32_t c, uint32_t d)
{
    return b + c + d;
}

uint32_t params_5(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e)
{
    return c + d + e;
}

uint32_t params_15(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e,
                   uint32_t f, uint32_t g, uint32_t h, uint32_t i, uint32_t j,
                   uint32_t k, uint32_t l, uint32_t m, uint32_t n, uint32_t o)
{
    return m + n + o;
}
uint32_t params_16(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e,
                   uint32_t f, uint32_t g, uint32_t h, uint32_t i, uint32_t j,
                   uint32_t k, uint32_t l, uint32_t m, uint32_t n, uint32_t o,
                   uint32_t p)
{
    return n + o + p;
}
uint32_t params_17(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e,
                   uint32_t f, uint32_t g, uint32_t h, uint32_t i, uint32_t j,
                   uint32_t k, uint32_t l, uint32_t m, uint32_t n, uint32_t o,
                   uint32_t p, uint32_t q)
{
    return o + p + q;
}
#pragma GCC diagnostic pop
