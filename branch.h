#ifndef BRANCH_H_KFTCAZBQ
#define BRANCH_H_KFTCAZBQ

#include "util.h"

/* TODO this doesn't seem to measure branch prediction; study and reimplement later.
bool pattern_constant(const uint32_t idx)
{
    (void)(idx);
    return true;
}

bool pattern_alternate(const uint32_t idx)
{
    return idx & 0x1;
}

bool pattern_random(const uint32_t idx)
{
    (void)(idx);
    return random_bool_slow();
}

class BenchBranch final: public Benchmark
{
    static constexpr size_t bool_mag = 12;
    static constexpr uint32_t bool_mag_mask = 0xFF8;
    static constexpr size_t bool_count = 1 << bool_mag;

public:
    explicit BenchBranch(bool (*pattern)(const uint32_t idx)) noexcept
        : pattern_(pattern)
    {
    }

    virtual void init() override
    {
        for (uint32_t i = 0; i < bool_count; ++i)
        {
            bools_[i] = pattern_(i);
        }
    }

    virtual void run(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base / 8;
        for (uint32_t baseIdx = 0, i = 0; i < iters; ++i, baseIdx += 8)
        {
            auto idx = baseIdx & bool_mag_mask;
            if (bools_[idx + 0]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 1]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 2]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 3]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 4]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 5]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 6]) { dummy_ += idx; } else { dummy_ += 2; }
            if (bools_[idx + 7]) { dummy_ += idx; } else { dummy_ += 2; }
        }
    }

private:
    bool (*pattern_)(const uint32_t idx);
    uint32_t dummy_ = 0;
    bool bools_[bool_count];
};
*/

#endif /* end of include guard: BRANCH_H_KFTCAZBQ */
