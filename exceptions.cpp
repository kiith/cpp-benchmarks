#include "exceptions.h"

void will_not_throw(uint32_t& idx, const uint32_t period) noexcept
{
    if (idx++ < period)
    {
        return;
    }
    idx = 1;
}

void may_throw(uint32_t& idx, const uint32_t period)
{
    if (idx++ < period)
    {
        return;
    }
    idx = 1;
    throw DummyException("blah");
}
