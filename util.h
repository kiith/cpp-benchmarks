#ifndef UTIL_H_INIVDSHY
#define UTIL_H_INIVDSHY

#include <cstdint>
#include <random>

inline bool random_bool_slow()
{
    std::random_device device;
    std::default_random_engine engine(device());
    std::uniform_int_distribution<uint32_t> distribution(0, 1);
    return distribution(engine) == 1;
}


#endif /* end of include guard: UTIL_H_INIVDSHY */
