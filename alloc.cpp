#include "alloc.h"

#include <cstdlib>

void* malloc_wrap(const size_t size)
{
    return malloc(size);
}

void free_wrap(void* const ptr)
{
    return free(ptr);
}

uint8_t* new_uint8_t(const size_t size)
{
    return new uint8_t[size];
}

void delete_uint8_t(uint8_t* const ptr)
{
    delete[] ptr;
}

Vec4* new_vec4(const size_t size)
{
    return new Vec4[size];
}

void delete_vec4(Vec4* const ptr)
{
    delete[] ptr;
}

