#include "polymorphism.h"


uint32_t PolyVirtualDerived1::call() { return counter_++; }
uint32_t PolyVirtualDerived2::call() { return counter_--; }
uint32_t PolyStatic::call() { return counter_++; }
