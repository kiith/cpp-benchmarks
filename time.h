#ifndef TIME_H_T3HQM4UG
#define TIME_H_T3HQM4UG


#ifdef __unix
    #include <time.h>
#endif

void cstdlib_time();

#ifdef __unix
    template <int TIME>
    void posix_clock_gettime()
    {
        struct timespec time;
        clock_gettime(TIME, &time);
    }
#endif



#endif /* end of include guard: TIME_H_T3HQM4UG */
