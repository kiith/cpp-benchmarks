#ifndef POLYMORPHISM_H_JU9QQ4MT
#define POLYMORPHISM_H_JU9QQ4MT

#include <cstdlib>
#include <cstdint>
#include <functional>

#include "benchmark.h"
#include "util.h"

class PolyVirtualBase
{
public:
    virtual uint32_t call() = 0;

    virtual ~PolyVirtualBase() {}
};

class PolyVirtualDerived1: public PolyVirtualBase
{
public:
    virtual uint32_t call();

private:
    uint32_t counter_ = 0;
};

class PolyVirtualDerived2: public PolyVirtualBase
{
public:
    virtual uint32_t call();

private:
    uint32_t counter_ = 0;
};


class PolyStatic final
{
public:
    uint32_t call();

private:
    uint32_t counter_ = 0;
};

class PolyStaticInlined final
{
public:
    uint32_t call() { return counter_++; }

private:
    uint32_t counter_ = 0;
};

class PolyFuncPtr final
{
public:
    PolyFuncPtr() { }

    PolyFuncPtr(uint32_t (*fptr)(PolyFuncPtr*)) : fptr_(fptr) {}

    uint32_t call() { return fptr_(this); }

    uint32_t counter_ = 0;

    uint32_t (*fptr_)(PolyFuncPtr*);
};

inline uint32_t polyFunc1(PolyFuncPtr* self)
{
    return self->counter_++;
}

inline uint32_t polyFunc2(PolyFuncPtr* self)
{
    return self->counter_--;
}

enum class Predictable: uint32_t
{
    No = 0,
    Yes
};



template <typename Poly>
class BenchPoly: public Benchmark
{
    static constexpr size_t object_mag = 15;
    static constexpr uint32_t object_mag_mask = 0x7FF8;
    static constexpr size_t object_count = 1 << object_mag;

public:
    explicit BenchPoly(const Predictable predictable) noexcept
        : predictable_(predictable)
    {
    }

    virtual void init() override
    {
        initObjects();
        for (uint32_t i = 0; i < object_count; ++i)
        {
            if (predictable_ == Predictable::Yes)
            {
                objectPtrs_[i] = object1_();
            }
            else
            {
                objectPtrs_[i] = random_bool_slow() ? object1_() : object2_();
            }
        }
    }

    virtual void run(const uint32_t iters_mult) override
    {
        const auto iters = iters_mult * iters_base / 8;
        for (uint32_t baseIdx = 0, i = 0; i < iters; ++i, baseIdx += 8)
        {
            // modulo 1 << object_mag, in increments of 8
            auto idx = baseIdx & object_mag_mask;
            dummy_ += objectPtrs_[idx + 0]->call(); dummy_ += objectPtrs_[idx + 1]->call();
            dummy_ += objectPtrs_[idx + 2]->call(); dummy_ += objectPtrs_[idx + 3]->call();
            dummy_ += objectPtrs_[idx + 4]->call(); dummy_ += objectPtrs_[idx + 5]->call();
            dummy_ += objectPtrs_[idx + 6]->call(); dummy_ += objectPtrs_[idx + 7]->call();
        }
    }

    virtual ~BenchPoly()
    {
        for (uint32_t i = 0; i < object_count; ++i)
        {
            delete objectPtrs_[i];
        }
    }

protected:
    std::function<Poly*()> object1_;
    std::function<Poly*()> object2_;

    virtual void initObjects() = 0;

private:
    Poly* objectPtrs_[object_count];
    uint32_t dummy_ = 0;

    Predictable predictable_;
};


class BenchPolyVirtual final: public BenchPoly<PolyVirtualBase>
{
protected:
    using BenchPoly::BenchPoly;

    virtual void initObjects() override
    {
        object1_ = [](){return new PolyVirtualDerived1();};
        object2_ = [](){return new PolyVirtualDerived2();};
    }
};

class BenchPolyFPtr final: public BenchPoly<PolyFuncPtr>
{
protected:
    using BenchPoly::BenchPoly;

    virtual void initObjects() override
    {
        object1_ = [](){ return new PolyFuncPtr(&polyFunc1);};
        object2_ = [](){ return new PolyFuncPtr(&polyFunc2);};
    }
};

/**
 * Where's the polymorphism? Parametrizing BenchPoly with PolyStatic
 * already *is* static polymorphism.
 *
 * One might protest that BenchPolyVirtual then has the overhead of both
 * static and dynamic polymorphism, but the point is: there is no added
 * overhead. Static polymorphism normally results in a direct call of
 * a non-virtual function - when we use a type with virtual functions
 * in static polymorphism, it's - a direct call of a virtual function.
 */
class BenchPolyStatic final: public BenchPoly<PolyStatic>
{
public:
    BenchPolyStatic()
        : BenchPoly(Predictable::Yes)
    { }

protected:
    virtual void initObjects() override
    {
        object1_ = [](){ return new PolyStatic();};
        object2_ = [](){ return new PolyStatic();};
    }
};

class BenchPolyStaticInlined final: public BenchPoly<PolyStaticInlined>
{
public:
    BenchPolyStaticInlined()
        : BenchPoly(Predictable::Yes)
    { }

protected:
    virtual void initObjects() override
    {
        object1_ = [](){ return new PolyStaticInlined();};
        object2_ = [](){ return new PolyStaticInlined();};
    }
};


#endif /* end of include guard: POLYMORPHISM_H_JU9QQ4MT */
